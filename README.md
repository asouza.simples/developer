# Teste Conta Simples

Olá tudo bem? Primeiramento muito obrigado, por tirar um tempinho para participar do processo da Conta Simples.

Atualmente estamos utilizando a stack abaixo.
- Serviços AWS (Lambda, Dynamodb, SQS, S3, Cognito...etc);
- Nossas integrações com frontend são via API REST e GRAPHQL;
- Frontend REACT
- Mobile REACTNATIVE

### Fazendo o teste você já pula algumas etepas do processo de seletivo.

Separamos 2 desafios, fique à vontade para escolher entre frontend e backend ou ambos.


# FRONTEND

- Descrever como fazemos para rodar o projeto ou URL da aplicação "deployada";
- As apis consumidas devem ser "mockadas";
- Criar integração mockada com api REST ou GRAPHQL;

1. Criar tela de Login para um Internet Banking;
2. Criar a Home do Internet Banking da Conta Simples (dados da empresa, saldo, grafico de entrada e saida);
3. Criar extrato da empresa e detalhe do extrato;
4. Permitir filtrar extrato por Tipo de Transação e flag de "Crédito" e "Débito";
5. Criar um tela para mostrar transações agrupadas por cartão, apresentar finalCartao e valor;
 
# BACKEND

- Descrever como fazemos para rodar o projeto ou URL da aplicação "deployada";
- A integração com banco de dados pode ser "mockada"
- Criar API REST/GRAPHQL de uma conta. Fique a vontade para utilizar a stack que você mais tem familiaridade.

1. Criar uma API de login;
2. Criar uma API que retorar o saldo da conta; 
3. Cria API de extrato da conta -> filtrar por Data de Transação e flag de "Crédito" e "Débito";
4. Criar API pra retornar o ultima transação realizada da empresa logada;
5. Endpoint para retornar transações agrupadas por cartão;

Nosso foco é identificar suas habilidade não só em programação, mas também comportamental, portanto, iremos analisar os seguintes critérios:

1. Capacidade em auto-aprendizado;
2. Comprometimento: Fazer mais do que o combinado;
3. Organização do código: Separação de módulos, view e model, back-end e front-end
4. Clareza: O README explica de forma resumida qual é o problema e como pode rodar a aplicação?
5. Assertividade: A aplicação está fazendo o que é esperado? Se tem algo faltando, o README explica o porquê?
6. Legibilidade do código (incluindo comentários)
7. Segurança: Existe alguma vulnerabilidade clara?
8. Cobertura de testes (Não esperamos cobertura completa)
9. Histórico de commits (estrutura e qualidade)
10. UX: A interface é de fácil uso e auto-explicativa

Importante ressaltar que todo o conhecimento adquirido neste desafio, será utilizado nas atividades do dia-a-dia aqui na Conta Simples.

Boa sorte!
